import React from 'react';
import '../assets/css/Pokedexitem.css'
import { useHistory } from 'react-router-dom';
import { useQuery } from '@apollo/client';

import { QUERY_TAG } from '../services/queries/Pokemon';

import Tag from '../components/Tag';
import iconpokeball from '../assets/images/pokeball.png';


const PokedexItem = (props) => {
    const history = useHistory()
    const Handler = () => {
        if (props.onClick) {
            props.onClick(props.nickname);
        }
        else {
            history.push(`/details/${props.name}`)
        }
    }
    const { loading, error, data } = useQuery(QUERY_TAG, { variables: { name: props.name } })
    if (loading || error) {
        return (
            <div class="pokedex-item" onClick={Handler}>
                <img src={props.image} width="150" height="150" />
                <h2>{props.name}</h2>
                <div class="container-tag">
                    Loading
                </div>
            </div>
        )
    }
    else if (data) {
        const dataTag = data.pokemon.types.map((dt, index) => {
            return <Tag name={dt.type.name} key={index} />
        })

        return (
            <div class="pokedex-item" onClick={Handler}>
                <img src={props.image} width="150" height="150" />
                <h2 style={{ marginBottom: "0px" }}>{props.name}</h2>
                {props.nickname ? <h4 style={{ margin: "0", color: "lightseagreen" }}>( {props.nickname} )</h4> : ''}
                {props.qty ? <h4 style={{ margin: "0", color: "lightseagreen", display: "flex", alignItems: "center", justifyContent: "center" }}><img src={iconpokeball} style={{ width: "20px", height: "20px" }} /> {props.qty} </h4> : ''}
                <div class="container-tag" style={{ marginTop: "15px" }}>
                    {dataTag}
                </div>
                {props.nickname ? <div class="btn-delete">Release</div> : ''}
            </div >
        )
    }
}

export default PokedexItem