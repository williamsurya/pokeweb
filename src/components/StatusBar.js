import React from 'react';
import '../assets/css/StatusBar.css'

const StatusBar = (props) => {

    const items = [];

    for (var i = 0; i < 10; i++) {
        if (i < (10 - props.value)) {
            items.push(<div class="block"></div>);
        }
        else {
            items.push(<div class="block active"></div>);
        }
    }


    return (
        <div class="statusbar">
            {items}
            <div>{props.name}</div>
        </div>
    )
}

export default StatusBar