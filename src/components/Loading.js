import React from 'react';
import '../assets/css/Loading.css'

const Loading = (props) => {
    return (
        <div class="container">
            <div class="loader" >
                <div class="circle" id="a"></div>
                <div class="circle" id="b"></div>
                <div class="circle" id="c"></div>
            </div>
            <div class="caption">Please Wait...</div>
        </div>
    )

}

export default Loading