import React, { useState } from 'react';
import styled from '@emotion/styled'

import { AddPokemon, getPokemon } from '../services/providers/Pokemon';
import { useHistory } from 'react-router-dom';


const Gotcha = (props) => {


    const Warn = styled.div`
        padding: 10px 30px;
        outline: none;
        border: none;
        border-radius: 6px;
        font-weight: 500;
        font-size: 10pt;
        color:red;
    `;

    const [Valid, setValid] = useState();
    const history = useHistory();
    const onSubmitHandle = (e) => {
        e.preventDefault()
        const newData = {
            id: props.id,
            nickname: e.target.nickname.value.trim(),
            pokemon: props.name,
        }

        const find = getPokemon.some((pokemon) => {
            return pokemon.nickname.toLowerCase() === e.target.nickname.value.trim().toLowerCase()
        })

        setValid(!find)

        if (!find) {
            AddPokemon(newData);
            history.go()
        }
    }

    return (
        <div class="container">
            <div class="container-form" style={{ height: "650px", position: "relative", maxWidth: "500px", margin: "50px auto", borderRadius: "25px", backgroundColor: "whitesmoke", padding: "0px 25px" }}>
                <img src={props.image} width="250" style={{ margin: "50px auto" }} />
                <h1 style={{ margin: "0px" }}>Congratulation you got {props.name}</h1>
                <h3>Please give your pokemon nickname</h3>
                <form onSubmit={onSubmitHandle}>
                    <input type="text" name="nickname" style={{ maxWidth: "300px", margin: "auto", height: "25px", borderRadius: "25px", padding: "5px 15px" }} placeholder="Nickname" />
                    {Valid === false ? <Warn>Please use different name!</Warn> : ''}
                    <button class="button" style={{ width: "100%", border: "none", height: "35px", padding: "5px 15px", marginTop: "15px" }}>Save</button>
                </form>
            </div>
        </div>
    )

}

export default Gotcha