
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './App.css';

import Navbar from './components/Navbar';
import Pokedex from './pages/Pokedex';
import PokeDetail from './pages/PokeDetail';
import MyPokemon from './pages/MyPokemon';
import Loading from './components/Loading';
import { Suspense } from "react";


function App() {
  return (
    <Suspense fallback={<Router><Navbar /><Loading /></Router>}>
      <Router>
        <div className="App">
          <Navbar />
          <Switch>
            <Route exact path="/">
              <Pokedex />
            </Route>
            <Route exact path="/details/:name">
              <PokeDetail />
            </Route>
            <Route exact path="/my-pokemon">
              <MyPokemon />
            </Route>
          </Switch>
        </div>
      </Router>
    </Suspense>
  );
}

export default App;
