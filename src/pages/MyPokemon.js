import React, { useState } from 'react';
import '../assets/css/Pokedex.css';
import styled from '@emotion/styled';

import PokedexItem from '../components/PokedexItem';
import { DiscardPokemon, getPokemon } from '../services/providers/Pokemon';
import { useHistory } from 'react-router-dom';
import nopokemon from '../assets/images/no-pokemon.png';

const MyPokemon = () => {

    const data = getPokemon
    const history = useHistory()
    const [Status, setStatus] = useState('idle');

    const Toast = styled.div`
        visibility: hidden;
        min-width: 250px;
        margin-left: -125px;
        background-color: #ed5564;
        color: #fff;
        text-align: center;
        border-radius: 12px;
        padding: 9px;
        position: fixed;
        z-index: 1;
        left: 50%;
        bottom: 30px;
        
        &.show {
            visibility: visible;
            animation: fadein 0.3s, fadeout 0.3s 1s;
            animation-fill-mode: forwards;
        }
        &.success{
            background-color:green;
            visibility: visible;
            animation: fadein 0.3s, fadeout 0.3s 1s;
            animation-fill-mode: forwards;
        }

        @keyframes fadein {
            from {bottom: 0; opacity: 0;}
            to {bottom: 100px; opacity: 1;}
        }

        @keyframes fadeout {
            from {bottom: 100px; opacity: 1;}
            to {bottom: 0; opacity: 0;}
        }
    `

    const OnDelete = async (nickname) => {
        DiscardPokemon(nickname)
        setStatus("release")
        await setTimeout(() => {
            history.go(0)
            console.log(getPokemon)
        }, 1000);
    }

    if (data) {
        if (data.length > 0) {
            const dataPokemons = data.map(({ id, nickname, pokemon }) => {
                const Image = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${id}.svg`
                return <PokedexItem
                    key={nickname}
                    nickname={nickname}
                    id={id}
                    name={pokemon}
                    image={Image}
                    onClick={OnDelete}
                />
            })
            return (
                <div class="container">
                    <h1>My Pokemon</h1>
                    {Status === 'release' ? <Toast className='success'>Your pokemon released to the wild.</Toast> : ''}
                    <div class="container-item" style={{ marginBottom: "100px" }}>
                        {dataPokemons}
                    </div>
                </div>
            )
        }
        else {
            return (
                <div class="container">
                    <h1>My Pokemon</h1>
                    <img src={nopokemon} style={{ margin: "25px auto 0px auto", maxWidth: "320px" }} />
                    <h3 style={{ marginBottom: "0", color: "teal" }}>You don't have any pokemon.</h3>
                    <h5 style={{ margin: "0" }}>Go catch them on pokedex</h5>
                </div>
            )
        }
    }
    else {
        return (
            <div>
                ERROR
            </div>
        )
    }
}

export default MyPokemon;