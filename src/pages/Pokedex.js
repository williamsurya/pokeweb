import React, { useState } from 'react';
import '../assets/css/Pokedex.css'
import { useQuery } from '@apollo/client';

import Loading from '../components/Loading';
import { QUERY_LIST } from '../services/queries/Pokemon';
import { getPokemon } from '../services/providers/Pokemon';
const Pokedex = () => {

    const [ItemOffset] = useState(0)
    const [ItemLimit, SetItemLimit] = useState(12)
    const { loading, error, data } = useQuery(QUERY_LIST, { variables: { limit: ItemLimit, offset: ItemOffset } })

    const PokedexItem = React.lazy(() => import('../components/PokedexItem'));

    const OnloadMore = async () => {
        SetItemLimit(data.pokemons.results.length + ItemLimit)
    }

    if (loading) {
        return (
            <Loading />
        )
    }
    else if (error) {
        return (
            <div>
                ERROR
            </div>
        )
    }
    else if (data) {
        const injectList = data.pokemons.results.map((pokemon) => {
            const total = getPokemon.filter(({ id }) => {
                return pokemon.id === id
            })
            return Object.assign({ ...pokemon }, { qty: total.length })
        })

        let dataPokemons = injectList.map(({ id, name, dreamworld, qty }) => (
            < PokedexItem
                key={name}
                id={id}
                name={name}
                qty={qty}
                image={dreamworld}
            />
        ))
        return (
            <div class="container">
                <h1>Pokedex</h1>
                <div class="container-item">
                    {dataPokemons}
                </div>
                <div onClick={OnloadMore} style={{ backgroundColor: "teal", width: "150px", margin: "10px auto 100px auto", color: "white", fontWeight: "bold", borderRadius: "15px", padding: "5px 0px", cursor: "pointer" }}>Load More</div>
            </div>
        )
    }
}

export default Pokedex;