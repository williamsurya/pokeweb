import React, { useState } from 'react';
import '../assets/css/PokeDetail.scss';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import styled from '@emotion/styled';

import Tag from '../components/Tag';
import StatusBar from '../components/StatusBar';
import Loading from '../components/Loading';
import Gotcha from '../components/Gotcha';
import { QUERY_DETAIL } from '../services/queries/Pokemon';

const PokeDetail = () => {

    const { name } = useParams();
    const { loading, error, data } = useQuery(QUERY_DETAIL, { variables: { name: name } })
    const [Status, setStatus] = useState('idle');

    const [stylePokeBall, setStylePokeBall] = useState({ opacity: 0, animation: "unset" });
    const [stylePokemon, setStylePokemon] = useState({ opacity: 1, animation: "unset" });
    const [stylePokeBallButton, setStylePokeBallButton] = useState({ animation: "blink 1.25s infinite" });
    // let stylePokeBall = { opacity: 0 };


    const Toast = styled.div`
        visibility: hidden;
        min-width: 250px;
        margin-left: -125px;
        background-color: #ed5564;
        color: #fff;
        text-align: center;
        border-radius: 12px;
        padding: 9px;
        position: fixed;
        z-index: 1;
        left: 50%;
        bottom: 30px;
        
        &.show {
            visibility: visible;
            animation: fadein 0.3s, fadeout 0.3s 1s;
            animation-fill-mode: forwards;
        }
        &.success{
            background-color:green;
            visibility: visible;
            animation: fadein 0.3s, fadeout 0.3s 1s;
            animation-fill-mode: forwards;
        }

        @keyframes fadein {
            from {bottom: 0; opacity: 0;}
            to {bottom: 100px; opacity: 1;}
        }

        @keyframes fadeout {
            from {bottom: 100px; opacity: 1;}
            to {bottom: 0; opacity: 0;}
        }
    `


    const Catch = async (e) => {
        e.preventDefault()
        setStylePokeBall({ opacity: 1, animation: "fall .25s ease-in-out, shake 1.25s cubic-bezier(.36,.07,.19,.97) 3" });
        setStylePokemon({ opacity: 0 });
        setStylePokeBallButton({ animation: "blink 1s infinite" });

        setStatus('idle')
        const isCatched = Boolean(Math.round(Math.random()))
        // const isCatched = false
        await setTimeout(async () => {
            if (isCatched) {
                console.log("dapat")
                setStylePokeBallButton({ animation: "unset" });
                await setStatus('success');
                await setTimeout(async () => {
                    await setStatus('gotcha');
                }, 1000)
            } else {
                console.log('gagal')
                setStylePokeBall({ opacity: 0, animation: "unset" });
                setStylePokemon({ opacity: 1 });
                await setStatus('failed')
            }
            console.log('Status', Status)
        }, 3500)

    }
    if (loading) {
        return (
            <Loading />
        )
    }
    else if (data) {
        const Image = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${data.pokemon.id}.svg`
        if (Status == 'gotcha') {
            return (
                <Gotcha image={Image} name={name} id={data.pokemon.id} />
            )
        }
        else {
            console.log(data);

            const dataAbilities = data.pokemon.abilities.map((dt, index) => {
                return <Tag name={dt.ability.name} key={index} />
            })

            const dataType = data.pokemon.types.map((dt, index) => {
                return <Tag name={dt.type.name} key={index} />
            })

            const dataStats = data.pokemon.stats.map((stat, index) => {
                const name = stat.stat.name;
                const val = (stat.base_stat / 25).toFixed(0);
                return <StatusBar
                    key={index}
                    name={name}
                    value={val}
                />
            })

            const dataMoves = data.pokemon.moves.map((dt, index) => {
                return <Tag name={dt.move.name} key={index} margin="8px 5px"/>
            })

            return (
                <div class="container">
                    {Status === 'failed' ? <Toast className='show'>Oops, you miss!</Toast> : ''}
                    {Status === 'success' ? <Toast className='success'>Gotcha, you got pokemon!</Toast> : ''}
                    <div class="profile">
                        <div class="left-side">
                            <div class="pokeball" style={stylePokeBall}>
                                <div id="pokeball_button" class="pokeball__button" style={stylePokeBallButton}></div>
                            </div>
                            <img id="pokemon" src={Image} style={stylePokemon}></img>
                        </div>
                        <div class="right-side">
                            <h3>
                                {name.toUpperCase()}
                            </h3>
                            <div class="biodata">
                                <div class="item">
                                    <h5>Height</h5>
                                    <span>{data.pokemon.height} "</span>
                                </div>
                                <div class="item">
                                    <h5>Weight</h5>
                                    <span>{data.pokemon.weight} g</span>
                                </div>
                            </div>
                            <div style={{ display: "flex", flexWrap: "wrap" }}>
                                <div style={{ flexBasis: "50%" }}>
                                    <h5 class="label">Type</h5>
                                    <div class="container-tag">
                                        {dataType}
                                    </div>
                                </div>
                                <div style={{ flexBasis: "50%" }}>
                                    <h5 class="label">Abilities</h5>
                                    <div class="container-tag">
                                        {dataAbilities}
                                    </div>
                                </div>
                            </div>
                            <div class="container-btn">
                                <div class="button" onClick={Catch}>Catch</div>
                            </div>
                        </div>
                    </div>
                    <div class="status">
                        <h3>Stats</h3>
                        <div class="container-status">
                            {dataStats}
                        </div>
                    </div>
                    <div>
                        <h3>Moves</h3>
                        <div style={{ display: "flex", flexWrap: "wrap", margin: "auto", width: "80%", justifyContent: "center" }}>
                            {dataMoves}
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default PokeDetail;