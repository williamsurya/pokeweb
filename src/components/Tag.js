import React from 'react';
import '../assets/css/Tag.css'


const Tag = (props) => {

    let tagColor = "lightgray";
    if (props.name == "grass") {
        tagColor = "green";
    }
    else if (props.name == "poison") {
        tagColor = "purple";
    }
    else if (props.name == "water") {
        tagColor = "blue";
    }
    else if (props.name == "flying") {
        tagColor = "lightseagreen";
    }
    else if (props.name == "bug") {
        tagColor = "darkgreen";
    }
    else if (props.name == "fire") {
        tagColor = "red";
    }
    else {
        tagColor = "teal";
    }

    return (
        <div class="tag" style={{ backgroundColor: tagColor, margin: props.margin }}>
            {props.name.toUpperCase()}
        </div>
    )
}

export default Tag